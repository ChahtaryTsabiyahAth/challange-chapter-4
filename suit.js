//computer//
function getComputerChoose() {
const comp = Math.random();
if( comp < 0.34 ) return 'compPaper';
if( comp >= 0.34 && comp < 0.67 ) return 'compScissors';
return 'compRock';
}

//rules//
function getResult(comp, player) {
if( player == 'playerPaper' ) return ( comp == 'compRock' ) ? 'Win' : 'Lose';
if( player == comp ) return 'Draw';
if( player == 'playerScissors')  return ( comp == 'compRock' ) ? 'Lose' : 'Win';
if( player == 'playerRock' ) return ( comp == 'compPaper' ) ? 'Lose' : 'Win';
}

//result//
const pRock = document.querySelector( '.playerRock');
pRock.addEventListener( 'click', function() {
    const computerChoose = getComputerChoose();
    const playerChoose = pRock.className;
    const result = getResult(computerChoose, playerChoose);
    console.log('comp : ' + computerChoose);
    console.log('player : ' + playerChoose);
    console.log('result : ' + result);
    
});

const pPaper = document.querySelector( '.playerPaper');
pPaper.addEventListener( 'click', function() {
    const computerChoose = getComputerChoose();
    const playerChoose = pPaper.className;
    const result = getResult(computerChoose, playerChoose);
    console.log('comp : ' + computerChoose);
    console.log('player : ' + playerChoose);
    console.log('result : ' + result);
    
});

const pScissors = document.querySelector( '.playerScissors');
pScissors.addEventListener( 'click', function() {
    const computerChoose = getComputerChoose();
    const playerChoose = pScissors.className;
    const result = getResult(computerChoose, playerChoose);
    console.log('comp : ' + computerChoose);
    console.log('player : ' + playerChoose);
    console.log('result : ' + result);

});